<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <link rel="icon" href="/favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome to Bizmate Weather App</title>
    <script type="module" crossorigin src="/assets/index-9eeddc0c.js"></script>
    <link rel="stylesheet" href="/assets/index-fb0ebe57.css">
  </head>
  <body>
    <div id="app"></div>
    
  </body>
</html>
