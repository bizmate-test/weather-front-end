export const 
systemMessages={
    //about emails
    emailIsRequired:"Email is required.",
    emailShouldBeValid:"Please input a valid email.",

    //about password
    passwordIsRequired:"Password is required.",
    resetCodeIsRequired:"Reset Code is required.",
    firstNameIsRequired:"First Name is required.",
    lastNameIsRequired:"Last Name is required.",
    countryIsRequired:"Country is required.",
    subnationalEntiryIsRequired:"State/Region/Province is required.",
    addressIsRequired:"Address is required.",
    activationCodeIsRequired:"Activation code is required.",
    cityIsRequired:"City is required.",
    zipPostalCodeIsRequired:"Zip/Postal Code is required.",
    phoneIsRequired:"Phone is required.",
    oldPasswordIsRequired:"Old Password is Required",
    exportSuccess: "Successfully exported the data. Downloading...",
}