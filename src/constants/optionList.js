export const 
optionLists={

   YesNo:[{
        value: '',
        label: 'All',
        },
        {
        value: '1',
        label: 'Yes',
        },{
        value: '0',
        label: 'No'
        }],

    ActiveInactive:[{
            value: '',
            label: 'All',
            },
            {
            value: '1',
            label: 'Active',
            },{
            value: '0',
            label: 'Inactive'
            }]      
}