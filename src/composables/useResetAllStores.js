import { ref } from 'vue'
import { useProductStore } from "@/stores/productStore";
import { useCategoryStore } from "@/stores/categoryStore";
import { useSupplierStore } from "@/stores/supplierStore";
import { useBrandStore } from "@/stores/brandStore";
import { useStoreStore } from "@/stores/storeStore";

export function useResetAllStores() {
  const count = ref(0)

 //Instantiate first the store
const productStore = useProductStore()
const categoryStore = useCategoryStore()
const brandStore = useBrandStore()
const supplierStore = useSupplierStore()
const storeStore = useStoreStore()

const resetAllStores = () => {
  productStore.reset()
  categoryStore.reset()
  brandStore.reset()
  supplierStore.reset()
  storeStore.reset()
};

  return {
    count,
    resetAllStores
  }
}