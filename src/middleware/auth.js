import cookies from 'js-cookie';
import router from "../router";
import axios from 'axios'

export default function auth({ next }) {
    const token = cookies.get('igo-x-access-token');
    if(!token) {
        return router.push({ name: 'loginmain' }) 
    } else {
        axios.post("/auth/get-login-status", {}, {
            headers: {"Authorization" : `Bearer ${cookies.get('igo-x-access-token')}`}
        })
        .then(res => {
            let data = res.data
            if(!data.login_status) {
                cookies.remove('igo-x-access-token')
                cookies.remove('user-data')
                return router.push({ name: 'loginmain' }) 
            }
        })
        .catch(err => {
            cookies.remove('igo-x-access-token')
            cookies.remove('user-data')
            return router.push({ name: 'loginmain' }) 
        });
    }
  
    return next();
}