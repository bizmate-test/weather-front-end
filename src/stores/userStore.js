import { ref, reactive } from 'vue';
import { defineStore } from 'pinia';
import axios from "axios";
import { useNotification } from '../composables/useNotification';
import { cookieData, systemMessages } from '@/constants';


export const useUserStore = defineStore('userStore', () => {

 //data *************************************/  
  const userDataSource = ref([]); 
  const userDataSourceMeta = ref([]);

  const systemMessage = systemMessages
  

  const viewUserFormRequest=reactive({
    page: 1,
    per_page: parseInt(import.meta.env.VITE_DEFAULT_PAGINATION_SIZE),
    filter_allcolumn: null,
    filter_activatewildcard: null,
    filter_id: null,
    filter_first_name: null,
    filter_last_name: null,
    filter_email: null,
    filter_phone: null,
    filter_address1: null,
    filter_address2: null,
    filter_city: null,
    filter_subnational_entity: null,
    filter_zip_postal_code: null,
    filter_country_id: null,
    filter_role_id: null,
    filter_created_at: null,

    sort_id: null,
    sort_first_name: null,
    sort_last_name: null,
    sort_email: null,
    sort_phone: null,
    sort_address1: null,
    sort_address2: null,
    sort_city: null,
    sort_subnational_entity: null,
    sort_zip_postal_code: null,
    sort_country_id: null,
    sort_role_id: null,
    sort_created_at: null,

    export_to: '',
  })

  const storeUserFormRequest=reactive({
    id: null,
    first_name: null,
    last_name: null,
    email: null,
    phone: null,
    address1: null,
    address2: null,
    city: null,
    subnational_entity: null,
    zip_post_code: null,
    country_id: null,
    role_id: null,
  })

//   const updateUserFormRequest=reactive({
//     id: null,
//     first_name: null,
//     last_name: null,
//     email: null,
//     phone: null,
//     address1: null,
//     address2: null,
//     city: null,
//     subnational_entity: null,
//     zip_post_code: null,
//     country_id: null,
//     role_id: null,
//   })

const updateUserFormRequest=ref('')

  const deleteUserFormRequest=reactive({
    page: 1,    
    sort_created_at: null,
    export_to: '',
  })

  const restoreUserFormRequest=reactive({
    page: 1,    
    sort_created_at: null,
    export_to: '',
  })

  const userLoading = ref(false)
  const updateUserFormRequestLoading = ref(false)

  //const landing_page_url = ref('/dashboard')
  const { openNotificationWithIcon } = useNotification()


  //getters *********************************/
  //const doubleCount = computed(() => count.value++ )

  //methods ********************************/
  function listUser(){
    userLoading.value = true; 
    userDataSource.value = []   
      axios
        .post("/user",viewUserFormRequest, {
          headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` },
        })     
        .then((res) => { 
          //save the retrieved data to userDataSource state       
          userDataSource.value = res.data.data 
          userDataSourceMeta.value = res.data.meta
          userDataSourceMeta.value['from'] = userDataSourceMeta.value['from'].toLocaleString()
          userDataSourceMeta.value['to'] = userDataSourceMeta.value['to'].toLocaleString()
          userDataSourceMeta.value['total'] = userDataSourceMeta.value['total'].toLocaleString()
          userLoading.value = false;                    
          //openNotificationWithIcon('success',res.data.message);      
          
          })
        .catch((err) => {
          userLoading.value = false;  
          openNotificationWithIcon('error',err.response.data.message);           
        });   
  }

  function storeUser(){   
      axios
        .post("/user/store",storeUserFormRequest, {
          headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` },
        })     
        .then((res) => { 
          //save the retrieved data to userDataSource state       
          //userDataSource.value = res.data.data 
          //userDataSourceMeta.value = res.data.meta
          //userLoading.value = false;     
          //update the list             
          openNotificationWithIcon('success',res.data.message,5); 
          listUser()     
          
          })
        .catch((err) => {
          //userLoading.value = false;  
          openNotificationWithIcon('error',err.response.data.message,5);           
        });   

  }

  function updateUser(id){  
    
    updateUserFormRequestLoading.value = true; 
    //userDataSource.value = []   
      axios
        .post(`/user/update/${id}`,updateUserFormRequest,{
          headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` },
        })     
        .then((res) => { 
          updateUserFormRequestLoading.value = false;                    
          openNotificationWithIcon('success',res.data.message,5);   
          listUser()   
          
          })
        .catch((err) => {
            updateUserFormRequestLoading.value = false;  
          openNotificationWithIcon('error',err.response.data.message,5);
        });   
  }

  function updateUserProfile(){  
    
    updateUserFormRequestLoading.value = true; 
    //userDataSource.value = []   
      axios
        .post(`/user/update-profile`,updateUserFormRequest.value,{
          headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` },
        })     
        .then((res) => { 
          updateUserFormRequestLoading.value = false;                    
          openNotificationWithIcon('success',res.data.message,5);            
          })
        .catch((err) => {
            updateUserFormRequestLoading.value = false;  
          openNotificationWithIcon('error',err.response.data.message,5);
        });   
  }

  function showUserProfile(){  
    
    //userDataSource.value = []   
      axios
        .post(`/user/show-profile`,null,{
          headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` },
        })     
        .then((res) => {  
           updateUserFormRequest.value=res.data.data   
           console.log('burt',updateUserFormRequest.value['first_name'])   
          //openNotificationWithIcon('success',res.data.message,5);            
          })
        .catch((err) => {
          openNotificationWithIcon('error',err.response.data.message,5);
        });   
  }


  function deleteUser(id){   
    userLoading.value = true; 
    //userDataSource.value = []   
      axios
        .get(`/user/delete/${id}`,{
          headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` },
        })     
        .then((res) => { 
          //save the retrieved data to userDataSource state       
          //userDataSource.value = res.data.data 
          //userDataSourceMeta.value = res.data.meta
          userLoading.value = false;                    
          openNotificationWithIcon('success',res.data.message,5); 
          listUser()     
          
          })
        .catch((err) => {
          userLoading.value = false;  
          openNotificationWithIcon('error',err.response.data.message);           
        });    
  }

  function restoreUser(){    
  }


  return { 
    userLoading,
    userDataSource, 
    userDataSourceMeta,
    viewUserFormRequest,
    storeUserFormRequest,
    updateUserFormRequest,
    deleteUserFormRequest,
    restoreUserFormRequest,
    updateUserFormRequestLoading,

    showUserProfile,
    updateUserProfile,
    listUser, 
    storeUser, 
    updateUser, 
    deleteUser,  
    restoreUser,
 
  }
})
